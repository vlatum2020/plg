import resolve from "rollup-plugin-node-resolve";
import typescript from "rollup-plugin-typescript2";
import copy from "rollup-plugin-copy";
import { PLUGIN_VERSION } from "./src/environments/version";

//const pluginPath = `../publish/${PLUGIN_VERSION.name}-${PLUGIN_VERSION.version}`;
const pluginPath = `./../testapp/src/assets/`;
const plugini18nPath = `${pluginPath}/i18n`;

export default {
	input: "src/main-plugin.ts",
	output: {
		file: `${pluginPath}/${PLUGIN_VERSION.name}-${PLUGIN_VERSION.version}.bundle.js`,
		format: "system",
	},
	plugins: [
		resolve({
			// pass custom options to the resolve plugin
			customResolveOptions: {
				moduleDirectory: "node_modules",
			},
		}),
		typescript({
			typescript: require("typescript"),
			//objectHashIgnoreUnknownHack: true,
			clean: true,
		}),
		// commonjs(),
		// copy({
		// 	targets: ["assets/i18n"],
		// 	outputFolder: "i18n",
		// }),
		// copy({
		// 	targets: [
		// 		// { src: "src/index.html", dest: "dist/public" },
		// 		// {
		// 		// 	src: ["assets/fonts/arial.woff", "assets/fonts/arial.woff2"],
		// 		// 	dest: "dist/public/fonts",
		// 		// },
		// 		{ src: "assets/i18n/**/*", dest: "dist" },
		// 	],
		// }),
		//uglify()
	],
	external: [
		"plugins-core",
		"@angular/core",
		"@angular/forms",
		"@angular/common",
		"@angular/common/http",
		"@angular/animations",
		"@angular/platform-browser",
		"@angular/platform-browser-dynamic",
		"@ngx-translate",
		"@ngx-translate/core",
		"@ngx-translate/http-loader",
		'ngx-bootstrap/tooltip',
		'ngx-bootstrap/tabs',
		'ngx-bootstrap/modal',
		'ngx-bootstrap/dropdown',
		'ngx-bootstrap/accordion',
		'ngx-bootstrap/alert',
		'ngx-bootstrap/buttons',
		'ngx-bootstrap/collapse',
		'ngx-bootstrap/tooltip',
		'ngx-bootstrap/typeahead',
		"file-saver",
		"xlsx",
		"rxjs",
		"rxjs/internal/Subject",
		"ngx-bootstrap",
		"jqwidgets-ng/jqxgrid",
		"jqwidgets-ng/jqxdatetimeinput",
		"jqwidgets-ng/jqxdropdownlist",
		"jqwidgets-ng/jqxchart",
		"jqwidgets-ng/jqxinput",
		"jqwidgets-ng/jqxcombobox",
		"jqwidgets-ng/jqxtooltip",
		"jqwidgets-ng/jqxloader",
		"jqwidgets-ng/jqxwindow",
		"jqwidgets-ng/jqxexpander",
		"angular2-draggable",
		"@angular/router"
	],
};
