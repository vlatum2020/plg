import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sub2',  
  template: `<p>sub 2 works!</p>`,
  styles: ['']
})
export class Sub2Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
