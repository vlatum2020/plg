import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sub',
  template: `<p>sub works!</p>`,
  styles: [''],
  providers: []
})
export class SubComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
