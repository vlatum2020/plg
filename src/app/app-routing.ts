import { RouterModule, Routes } from '@angular/router';
import { SubComponent } from './components/sub/sub.component';
import { Sub2Component } from './components/sub2/sub2.component';

export const ROUTE: Routes = 
[
  {path:"one", component: SubComponent},
  {path:"two", component: Sub2Component},
  {path:"plugin/one", component: SubComponent},
  {path:"plugin/two", component: Sub2Component},
];