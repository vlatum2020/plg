import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  template: `
  <div style="background-color: aqua;">
    <p> PLUGIN </p>
    <button routerLink="one"> plugin sub</button>
    <button routerLink="two"> plugin sub 2</button>
    <router-outlet></router-outlet>
  </div>
  `,
  styles: [``]
})
export class AppComponent {
  title = 'plagin1';
  constructor()
  {

  }
}
