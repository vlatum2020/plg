import { NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { AppComponent } from '../app.component';
import { SubComponent } from '../components/sub/sub.component';
import { Sub2Component } from '../components/sub2/sub2.component';

export const PLUGIN_ROUTE: Routes = 
[
  {path:"plg", component: AppComponent, 
  children:[
    {path:"one", component: SubComponent},
    {path:"two", component: Sub2Component},]  
}
];

@NgModule({
  imports: [RouterModule.forChild(PLUGIN_ROUTE)],
  exports: [RouterModule],
  providers:[]
})
export class AppRoutingModule { }
