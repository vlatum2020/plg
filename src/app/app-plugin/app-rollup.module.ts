import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-rollup-routing.module';
import { AppComponent } from '../app.component';
import { SubComponent } from '../components/sub/sub.component';
import { Sub2Component } from '../components/sub2/sub2.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    SubComponent,
    Sub2Component
  ],
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class Plugin1AppModule 
{
}
