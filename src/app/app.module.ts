import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SubComponent } from './components/sub/sub.component';
import { Sub2Component } from './components/sub2/sub2.component';

@NgModule({
  declarations: [
    AppComponent,
    SubComponent,
    Sub2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
