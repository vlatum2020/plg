import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ROUTE } from './app-routing';

@NgModule({
  imports: [RouterModule.forRoot(ROUTE)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
